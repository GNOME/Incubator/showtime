# Romanian translation for showtime.
# Copyright (C) 2024 showtime's COPYRIGHT HOLDER
# This file is distributed under the same license as the showtime package.
# Florentina Mușat <florentina.musat.28@gmail.com>,  2024.
#
msgid ""
msgstr ""
"Project-Id-Version: showtime main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/Incubator/showtime/-/"
"issues\n"
"POT-Creation-Date: 2024-09-11 13:31+0000\n"
"PO-Revision-Date: 2024-09-12 08:10+0300\n"
"Last-Translator: Florentina Mușat <florentina [dot] musat [dot] 28 [at] "
"gmail [dot] com>\n"
"Language-Team: Romanian <Romanian>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2);\n"
"X-Generator: Poedit 3.5\n"

#: data/org.gnome.Showtime.desktop.in:3
#: data/org.gnome.Showtime.metainfo.xml.in:6 showtime/main.py:452
#: showtime/gtk/window.blp:193
msgid "Showtime"
msgstr "Oră spectacol"

#: data/org.gnome.Showtime.desktop.in:4
msgid "Video Player"
msgstr "Player video"

#: data/org.gnome.Showtime.desktop.in:5
msgid "Play videos"
msgstr "Redă video-uri"

#: data/org.gnome.Showtime.desktop.in:16 showtime/gtk/help-overlay.blp:29
msgid "New Window"
msgstr "Fereastră nouă"

#: data/org.gnome.Showtime.metainfo.xml.in:7
msgid "Watch without distraction"
msgstr "Vizionează fără distracție"

#: data/org.gnome.Showtime.metainfo.xml.in:9
msgid ""
"Play your favorite movies and video files without hassle. Showtime features "
"simple playback controls that fade out of your way when you're watching, "
"fullscreen, adjustable playback speed, multiple language and subtitle "
"tracks, and screenshots — everything you need for a straightforward viewing "
"experience."
msgstr ""
"Redați filmele și fișierele video preferate fără probleme. Oră spectacol "
"dispune de comenzi simple de redare care se estompează atunci când "
"vizionați, ecran complet, viteză de redare ajustabilă, piste multiple de "
"limbă și subtitrări, și capturi de ecran — tot ce aveți nevoie pentru o "
"experiență de vizionare simplă."

#: data/org.gnome.Showtime.metainfo.xml.in:35
msgid "The app playing a video"
msgstr "Aplicația care redă un video"

#: data/org.gnome.Showtime.gschema.xml.in:12
msgid "What to show in the end timestamp position"
msgstr "Ce să se afișeze în poziția marcajului de timp final"

#: data/org.gnome.Showtime.gschema.xml.in:13
msgid "E.g. total duration of the video or the amount of time remaining"
msgstr "Adică durata totală a video-ului sau timpul rămas"

#: showtime/main.py:140
msgid "Playing a video"
msgstr "Se redă un video"

#. Translators: Replace this with your name for it to show up in the about dialog
#: showtime/main.py:427
msgid "translator_credits"
msgstr ""
"Florentina Mușat <florentina [dot] musat [dot] 28 [at] gmail [dot] com>, 2024"

#: showtime/mpris.py:289 showtime/window.py:593
msgid "Unknown Title"
msgstr "Titlu necunoscut"

#: showtime/window.py:140 showtime/gtk/window.blp:479
msgid "Play"
msgstr "Redă"

#: showtime/window.py:143
msgid "Pause"
msgstr "Pauză"

#: showtime/window.py:449
msgid "Details copied"
msgstr "Detalii copiate"

#: showtime/window.py:452
msgid "Copy Technical Details"
msgstr "Copiază detaliile tehnice"

#: showtime/window.py:478
msgid "The “{}” codecs required to play this video could not be found"
msgstr "Nu s-au putut găsi codec-urile „{}” necesare pentru a reda acest video"

#: showtime/window.py:499
msgid "No plugin available for this media type"
msgstr "Nu există niciun modul disponibil pentru acest tip de media"

#: showtime/window.py:505
msgid "Unable to install the required plugin"
msgstr "Nu s-a putut instala modulul necesar"

#: showtime/window.py:508
msgid "Install Plugin"
msgstr "Instalează modulul"

#: showtime/window.py:516
msgid "Installing…"
msgstr "Se instalează…"

#: showtime/window.py:524
msgid "“{}” codecs are required to play this video"
msgstr "„{}” codec-uri sunt necesare pentru a reda acest video"

#: showtime/window.py:604
msgid "Screenshot captured"
msgstr "Captură de ecran luată"

#: showtime/window.py:606
msgid "Show in Files"
msgstr "Arată în Fișiere"

#: showtime/window.py:668
msgid "Video"
msgstr "Video"

#: showtime/window.py:687
msgid "Subtitles"
msgstr "Subtitrări"

#. Translators: The variable is the number of channels in an audio track
#: showtime/window.py:753
msgid "Undetermined, {} Channel"
msgid_plural "Undetermined, {} Channels"
msgstr[0] "Nedeterminat, {} canal"
msgstr[1] "Nedeterminat, {} canale"
msgstr[2] "Nedeterminat, {} de canale"

#: showtime/window.py:758
msgid "Undetermined"
msgstr "Nedeterminat"

#: showtime/window.py:765
msgid "No Audio"
msgstr "Fără audio"

#: showtime/window.py:770
msgid "None"
msgstr "Nimic"

#: showtime/window.py:776
msgid "Undetermined Language"
msgstr "Limbă nedeterminată"

#: showtime/window.py:784
msgid "Add Subtitle File…"
msgstr "Adaugă un fișier de subtitrări…"

#: showtime/gtk/help-overlay.blp:11
msgid "General"
msgstr "Generale"

#: showtime/gtk/help-overlay.blp:14
msgid "Show Shortcuts"
msgstr "Arată scurtăturile"

#: showtime/gtk/help-overlay.blp:19
msgid "Open Video"
msgstr "Deschide video"

#: showtime/gtk/help-overlay.blp:24
msgid "Take Screenshot"
msgstr "Ia captura de ecran"

#: showtime/gtk/help-overlay.blp:34
msgid "Close Window"
msgstr "Închide fereastra"

#: showtime/gtk/help-overlay.blp:39
msgid "Quit"
msgstr "Ieșire"

#: showtime/gtk/help-overlay.blp:45
msgid "Volume"
msgstr "Volum"

#: showtime/gtk/help-overlay.blp:48
msgid "Increase Volume"
msgstr "Mărește volumul"

#: showtime/gtk/help-overlay.blp:53
msgid "Decrease Volume"
msgstr "Micșorează volumul"

#: showtime/gtk/help-overlay.blp:58 showtime/gtk/window.blp:129
#: showtime/gtk/window.blp:157
msgid "Mute/Unmute"
msgstr "Mut/Cu sunet"

#: showtime/gtk/help-overlay.blp:64
msgid "View"
msgstr "Vizualizare"

#: showtime/gtk/help-overlay.blp:67 showtime/gtk/window.blp:545
msgid "Toggle Fullscreen"
msgstr "Comută ecranul complet"

#: showtime/gtk/help-overlay.blp:72
msgid "Exit Fullscreen"
msgstr "Ieșire din ecran complet"

#: showtime/gtk/help-overlay.blp:78
msgid "Playback"
msgstr "Redare"

#: showtime/gtk/help-overlay.blp:81
msgid "Play/Pause"
msgstr "Redă/Pauză"

#: showtime/gtk/help-overlay.blp:86 showtime/gtk/window.blp:152
#: showtime/gtk/window.blp:469
msgid "Go Back 10 Seconds"
msgstr "Navighează înapoi 10 secunde"

#: showtime/gtk/help-overlay.blp:91 showtime/gtk/window.blp:153
#: showtime/gtk/window.blp:488
msgid "Go Forward 10 Seconds"
msgstr "Navighează înainte 10 secunde"

#: showtime/gtk/window.blp:6 showtime/gtk/window.blp:178
msgid "_Open…"
msgstr "_Deschide…"

#: showtime/gtk/window.blp:11 showtime/gtk/window.blp:171
msgid "Show in _Files"
msgstr "Arată în _Fișiere"

#: showtime/gtk/window.blp:19 showtime/gtk/window.blp:163
msgid "Take _Screenshot"
msgstr "Ia _captura de ecran"

#: showtime/gtk/window.blp:26
msgid "_Keyboard Shortcuts"
msgstr "Scurtături de _tastatură"

#: showtime/gtk/window.blp:27
msgid "_About Showtime"
msgstr "_Despre Oră spectacol"

#: showtime/gtk/window.blp:39
msgid "_Language"
msgstr "_Limba"

#: showtime/gtk/window.blp:43
msgid "_Subtitles"
msgstr "_Subtitrări"

#: showtime/gtk/window.blp:48 showtime/gtk/window.blp:158
msgid "_Repeat"
msgstr "_Repetă"

#: showtime/gtk/window.blp:66
msgid "Playback Speed"
msgstr "Viteză de redare"

#: showtime/gtk/window.blp:212 showtime/gtk/window.blp:578
msgid "Main Menu"
msgstr "Meniu principal"

#: showtime/gtk/window.blp:221
msgid "Watch Videos"
msgstr "Vizionează video-uri"

#: showtime/gtk/window.blp:222
msgid "Drag and drop videos here"
msgstr "Trageți și plasați video-uri aici"

#: showtime/gtk/window.blp:225
msgid "Open…"
msgstr "Deschide…"

#: showtime/gtk/window.blp:238
msgid "Unable to Play Video"
msgstr "Nu se poate reda video-ul"

#: showtime/gtk/window.blp:243
msgid "Missing Plugin"
msgstr "Lipsește un modul"

#: showtime/gtk/window.blp:285
msgid "Resume"
msgstr "Revenire"

#: showtime/gtk/window.blp:317
msgid "Start Again"
msgstr "Începe din nou"

#: showtime/gtk/window.blp:422
msgid "Toggle Duration/Remaining"
msgstr "Comută durata/rămas"

#: showtime/gtk/window.blp:458
msgid "Adjust Volume"
msgstr "Ajustează volumul"

#: showtime/gtk/window.blp:501
msgid "Playback Options"
msgstr "Opțiuni de redare"
